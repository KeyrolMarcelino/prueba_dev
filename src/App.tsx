import List from "./components/list/List";
import { useState } from "react";
import AppContext from "./Contex";
import Login from "./components/login/Login";
import PersistentDrawerLeft from "./components/navbar/Navbar";
import Container from "@material-ui/core/Container";
import { AddUser } from "./components/forms/Add";

const App = () => {
  const [authenticated, setaAuthenticated] = useState(false);
  const [Add, setAdd] = useState(false);

  const values = { authenticated, setaAuthenticated, Add, setAdd };

  return (
    <AppContext.Provider value={values}>
      <AppContext.Consumer>
        {({ authenticated }) => {
          if (authenticated) {
            return (
              <div>
                <PersistentDrawerLeft></PersistentDrawerLeft>
                <Container component="main" maxWidth="lg">
                  {Add ? <AddUser /> : <List />}
                </Container>
              </div>
            );
          }
          return (
            <div>
              <Login></Login>
            </div>
          );
        }}
      </AppContext.Consumer>
    </AppContext.Provider>
  );
};

export default App;
