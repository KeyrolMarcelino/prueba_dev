import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  item: [
    {
      id: 1,
      name: "Juan",
      PrimerApellido: "Ramirez",
      SegundoApellido: "Sanchez",
      Cédula: 67675372,
      Edad: 20,
      Género: "m",
      Dirección: ["calle1", "calle2"],
      Teléfono: 634221,
      Correo: "prueba@gmail.com",
      Passw: "123",
      EstadoCivil: "soltero",
      hijos: true,
      Nacimiento: "22/12/00",
    },
    {
      id: 2,
      name: "pedro",
      PrimerApellido: "Lopez",
      SegundoApellido: "Vega",
      Cédula: 335352,
      Edad: 14,
      Género: "m",
      Dirección: ["calle1", "calle2"],
      Teléfono: 2455,
      Correo: "prueba",
      Passw: "123",
      EstadoCivil: "casado",
      hijos: false,
      Nacimiento: "22/12/00",
    },
    
    {
      id: 3,
      name: "Maria",
      PrimerApellido: "Marcelino",
      SegundoApellido: "Arias",
      Cédula: 2562621,
      Edad: 40,
      Género: "f",
      Dirección: ["calle1", "calle2"],
      Teléfono: 2455,
      Correo: "maria",
      Passw: "abc",
      EstadoCivil: "casado",
      hijos: true,
      Nacimiento: "22/12/00",
    },
      
  ],
};

const reducer = createSlice({
  name: "users",
  initialState,
  reducers: {
    userAdded(state, action) {
      let existingUser = state.item.find(
        (user) => user.id === action.payload.id
      );

      while (existingUser) {
        action.payload.id = action.payload.id + 1;
        existingUser = state.item.find((user) => user.id === action.payload.id);
      }
      state.item.push(action.payload);
    },
    userDeleted(state, action) {
      const { id } = action.payload;
      const existingUser = state.item.find((user) => user.id === id);
      if (existingUser) {
        state.item = state.item.filter((user) => user.id !== id);
      }
    },
  },
});

export const { userAdded } = reducer.actions;
export const { userDeleted } = reducer.actions;

export default reducer.reducer;
