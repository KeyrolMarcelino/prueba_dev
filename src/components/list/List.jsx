import  useStyles  from "./style";
 import Grid from '@material-ui/core/Grid';
import { useSelector } from "react-redux";
import { useState } from "react";
import ListItem from "./listItem";
import { Pagination } from "@material-ui/lab";
import usePagination from "./pagination";


function List() {
//hooks y variables
    const users = useSelector((state) => state.users);

    const classes = useStyles();
     let [page, setPage] = useState(1);
     const PER_PAGE = 5;
   
     const count = Math.ceil(users.item.length / PER_PAGE);
     const DATA = usePagination(users.item, PER_PAGE);

     const handleChange = (e, p) => {
      setPage(p);
      DATA.jump(p);
    };
    return (
      <div>
        <Pagination
                count={count}
                size="large"
                page={page}
                variant="outlined"
                shape="rounded"
                onChange={handleChange}
              />
      <Grid container spacing={3} >
        
               {DATA.currentData().map(user => {
       return (
      <ListItem i={user.i} title={user.title} name={user.name} PrimerApellido={user.PrimerApellido} SegundoApellido={user.SegundoApellido} 
      Correo={user.Correo} Género={user.Género} id={user.id} />
       )
    })}
        
         

    </Grid>
    </div>
    );
}

export default List
