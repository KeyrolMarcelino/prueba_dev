import  useStyles  from "./style";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { useDispatch } from "react-redux";
import { Button } from '@material-ui/core';
import { userDeleted } from "../../reducer";
import { useState } from "react";
 


function ListItem(props: any) {
 
    const classes = useStyles();
    const [error, setError] = useState(false);

     const dispatch = useDispatch(); 
 

    //funcion para borrar usuario
    const deleteUser = (id: any) =>{
         
        dispatch(userDeleted({ id }));        
        setError(false);

       
    }
    
    return (
     <Grid item lg={3} key={props.i}>
       <Card className={classes.root}>
        
        <CardContent  >
          <Typography className={classes.title} color="textSecondary" gutterBottom>
           
            
            
          </Typography>
          <Typography variant="h5" component="h2">
          {`${props.name} ${props.PrimerApellido} ${props.SegundoApellido}`} 
          </Typography>
          <Typography className={classes.pos} color="textSecondary">
            {props.Correo}
          </Typography>
          <Typography variant="body2" component="p">
          {` ${props.Género =="m" ? "masculino" : "femenino"}`}<br />
              {`id: ${props.id} `}            
           

          </Typography>
        </CardContent>
       
      </Card>
      <Button onClick={() => deleteUser(props.id)}>Eliminar</Button>

     </Grid>
      
        
         

   
    );
}

export default ListItem
