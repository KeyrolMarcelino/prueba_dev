import AppContext from "../../Contex";
import { useContext, useState } from "react";
import useStyles from "./style";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { useSelector } from "react-redux";

function Login() {
  //hooks y variables
  const { authenticated, setaAuthenticated } = useContext(AppContext);
  const [username, setUsername] = useState(null);
  const [Passw, setPassw] = useState(null);
  const [mistake, SetMistake] = useState("null");
  const classes = useStyles();
  const users = useSelector((state) => state.users.item);

  function search(name: any, Passw: any, array: any) {
    for (var i = 0; i < array.length; i++) {
      if (array[i].Correo === name && array[i].Passw === Passw) {
        return true;
      }
    }
  }

  const login = (e) => {
    e.preventDefault();
    if (username && Passw) {
      if (search(username, Passw, users)) {
        setaAuthenticated(true);
      } else {
        SetMistake("Usuario o contraseña incorrecto");
      }
    } else {
      SetMistake("Favor llenar todos los campos");
    }
  };
  return (
    <div>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            login
          </Typography>

          {mistake != "null" ? (
            <h3 className={classes.h3}>{mistake}</h3>
          ) : (
            <h3 className={classes.h2}>Ingrese su Usuario y Contraseña</h3>
          )}

          <form className={classes.form} noValidate onSubmit={login}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Correo electronico"
              name="email"
              onChange={(e) => {
                setUsername(e.target.value);
              }}
            />
            <TextField
              type="password"
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="passw"
              label="Contraseña"
              name="passw"
              onChange={(e) => {
                setPassw(e.target.value);
              }}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Entrar
            </Button>
          </form>
        </div>
      </Container>
    </div>
  );
}

export default Login;
