import { makeStyles } from "@material-ui/core/styles";


const useStyles = makeStyles((theme) => ({
     
    form: {
      '& > *': {
        margin: theme.spacing(3),
        width: '25ch',
      },   
    },
    h3: {
      color: "red",
      margin: theme.spacing(0, 0, 0, 0),
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
      backgroundColor: "#439441",
       
    },
  }));

  export default useStyles;
