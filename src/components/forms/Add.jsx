import React from 'react';
 import AppContext from "../../Contex";
import { useState ,useContext} from "react";
import { userAdded } from "../../reducer";
import { useSelector,useDispatch } from "react-redux";
import  useStyles  from "./style";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox  from '@material-ui/core/Checkbox';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

export function AddUser() {
    //hooks y variables
  const dispatch = useDispatch(); 
  const { Add, setAdd } = useContext(AppContext);
  const [name, setName] = useState("");
  const [PrimerApellido, setPrimerApellido] = useState("");
  const [SegundoApellido, setSegundoApellido] = useState("");
  const [Cédula, setCédula] = useState("");
  const [Edad, setEdad] = useState(0);
  const [Género, setGénero] = useState("");
  const [Dirección, setDirección] = useState("");
  const [Teléfono, setTeléfono] = useState("");
  const [Correo, setCorreo] = useState("");
  const [Passw, setPassw] = useState("");
  const [EstadoCivil, setEstadoCivil] = useState("");
  const [hijos, setHijos] = useState(false);
  const [Nacimiento, setNacimiento] = useState("");
  const [error, setError] = useState(false);
  

  //handlres para cambio de valor en lso inputs
  const handleName = (e) => setName(e.target.value);
  const handlePrimerApellido = (e) => setPrimerApellido(e.target.value);
  const handleSegundoApellido = (e) => setSegundoApellido(e.target.value);
  const handleCédula = (e) => setCédula(e.target.value);
  const handleEdad = (e) => setEdad(e.target.value);
  const handleGénero = (e) => setGénero(e.target.value);;
  const handleDirección = (e) => setDirección(e.target.value);
  const handleTeléfono = (e) => setTeléfono(e.target.value);
  const handleCorreo = (e) => setCorreo(e.target.value);
  const handlePassw = (e) => setPassw(e.target.value);
  const handleEstadoCivil = (e) => setEstadoCivil(e.target.value);
  const handlehijos = (e) => setHijos(e.target.checked);
  const handleNacimiento = (e) => setNacimiento(e.target.value);
  const classes = useStyles();
  const usersAmount = useSelector((state) => state.users.item.length);

  //funcion para crear usuario
  const handleClick = () => {
    if (name, PrimerApellido ,SegundoApellido ,Cédula ,Edad ,Género ,Dirección ,Teléfono ,Correo ,Passw ,EstadoCivil , hijos ,Nacimiento) {
      dispatch(
        userAdded({
          id: usersAmount + 1,
      name ,
      PrimerApellido ,
      SegundoApellido ,
      Cédula ,
      Edad ,
      Género ,
      Dirección ,
      Teléfono ,
      Correo ,
      Passw ,
      EstadoCivil ,
      hijos ,
      Nacimiento ,
        })
      );
      setError(null);
      setAdd(false)
      
     } else {
      setError(<h3 className={classes.h3}>Porfavor llenar todos los campos</h3>);
    }
 
  };
  


return (
  
  <div className="container">
     
     {
      error && error ? <h3 className={classes.h3}>Porfavor llenar todos los campos</h3> : null
      }
    
    <form className={classes.form} >
      <TextField
      fullWidth
        className="u-full-width"
        type="text"
        id="standard-basic"    
        label="Nombre"     
        onChange={handleName}
        value={name}      />
      
       <TextField
       fullWidth
        className="u-full-width"
        type="text"
        id="PrimerApellidoInput"
        label="Primer Apellido"     
        onChange={handlePrimerApellido}
        value={PrimerApellido}
      />
       <TextField
       fullWidth
        className="u-full-width"
        type="text"
        id="SegundoApellidoInput"
        label="Segundo Apellido"     
        onChange={handleSegundoApellido}
        value={SegundoApellido}
      />
       <TextField
         className="u-full-width"
        type="number"
        id="CédulaInput"
        label="Cédula"
        onChange={handleCédula}
        value={Cédula}
      />
       <TextField
         className="u-full-width"
        type="number"
        id="EdadInput"
        label="Edad"
        onChange={handleEdad}
        value={Edad}
      />
      <FormControl>
      <InputLabel id="GéneroInputl">Género</InputLabel>
        <Select
          labelId="GéneroInputl"
          id="GéneroInput"
          value={Género}
          onChange={handleGénero}
        >
          <MenuItem value={"m"}>Masculino</MenuItem>
          <MenuItem value={"f"}>Femenino</MenuItem>
        </Select>
        </FormControl>
       <TextField
       fullWidth
        className="u-full-width"
        type="text"
        id="DirecciónInput"
        label="Dirección"
        onChange={handleDirección}
        value={Dirección}
      />
       <TextField
       fullWidth
        className="u-full-width"
        type="tel"
        id="TeléfonoInput"
        label="Teléfono"
        inputProps={{
          maxLength: 10,
        }}
        onChange={handleTeléfono}
        value={Teléfono}
      />
       <TextField
       fullWidth
        className="u-full-width"
        type="email"
        id="CorreoInput"
        label="Correo"
        onChange={handleCorreo}
        value={Correo}
      />
       <TextField
       fullWidth
        className="u-full-width"
        type="password"
        id="PasswInput"
        label="Contraseña"
        onChange={handlePassw}
        value={Passw}
      />
       <TextField
       fullWidth
        className="u-full-width"
        type="text"
        id="EstadoCivilInput"
        label="Estado Civil"
        onChange={handleEstadoCivil}
        value={EstadoCivil}
      />
        <FormControlLabel
        control={<Checkbox  checked={hijos} onChange={handlehijos} name="Hijos" />}
        label="Tiene hijos?"
      />
       <TextField
       fullWidth
        className="u-full-width"
        InputLabelProps={{ shrink: true }}
        label="Fecha de Nacimiento"

        type="date"
        id="NacimientoInput"
         
        onChange={handleNacimiento}
        value={Nacimiento}
      />
       
           <br/>
          <Button onClick={handleClick} variant="contained" color="primary"  className={classes.submit}>Crear</Button>
    </form>
  </div>
 
)}